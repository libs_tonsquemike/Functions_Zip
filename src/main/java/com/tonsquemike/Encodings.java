package com.tonsquemike;

import java.util.HashMap;
import java.util.Map;

public class Encodings {
    Encodings() {
    }

    public static String toJavaEncoding(String MozillaEncoding) {
        Map<String, String> encodings = new HashMap();
        encodings.put("UTF-8", "UTF-8");
        encodings.put("WINDOWS-1252", "Cp1252");
        return (String)encodings.get(MozillaEncoding);
    }
}