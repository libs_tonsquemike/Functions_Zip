package com.tonsquemike;


import libs.Archivos;
import libs.MyListArgs;
import libs.MySintaxis;
import org.mozilla.universalchardet.CharsetListener;
import org.mozilla.universalchardet.UniversalDetector;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/*
 * Here we will learn to read the file in Archive without extracting it.
 * File example:
 * -NOMBRE Cesar Estrada Palacios
   -NOMBRE_PRESENTACION Realidad Virtual en modelos arquitectónicos
   -NIVEL Maestría
   -CORREO cesarín0594@gmail.com
 */

public class ReadFileInArchive {
    String applicationPath;

    ReadFileInArchive(String[] args) {
        String  zipFileLocation;
        String  fileToFind;
        String  labelToFind;
        String  out;
        ZipFile zipfile = null;
        String  ConfigFile;
        MyListArgs Param;

        Param = new MyListArgs(args);
        ConfigFile = Param.ValueArgsAsString("-CONFIG", "");//Ruta donde se van a crear los individuos
        if (!ConfigFile.equals("")) {
            Param.AddArgsFromFile(ConfigFile);
        }

        String Sintaxis   = "[-APPLICATION_PATH:str] -IN:str -FILE_TO_FIND:str [-LABEL_TO_FIND:str] [-OUT:str]";

        MySintaxis Review = new MySintaxis(Sintaxis, Param);

        applicationPath   = Param.ValueArgsAsString  ("-APPLICATION_PATH" , "" );
        zipFileLocation   = Param.ValueArgsAsString  ("-IN"               , "" );
        fileToFind        = Param.ValueArgsAsString  ("-FILE_TO_FIND"     , "" );
        labelToFind       = Param.ValueArgsAsString  ("-LABEL_TO_FIND"    , "" );
        out               = Param.ValueArgsAsString  ("-OUT"              , "" );

        Scanner scanner   = new Scanner(System.in);
        File file         = new File(zipFileLocation);try{
        zipfile           = new ZipFile(file);}catch (Exception e){}

        ZipEntry zipentry;
        /*
        for (Enumeration<? extends ZipEntry> e = zipfile.entries(); e
                .hasMoreElements(); fileNumber++) {
            zipentry = e.nextElement();
            if (!zipentry.isDirectory()) {
                System.out.println(fileNumber + "-" + zipentry.getName());
            }
        }
        */
        //System.out.println("Enter the file name(With path) to see the content of the file ");

        InputStream inputstream = null;
        String str = "not found";
        String encoding;
        InputStream tmp = null;
        String token = "";
        for (Enumeration<? extends ZipEntry> e = zipfile.entries(); e.hasMoreElements();) {

            try {
                zipentry = e.nextElement();

                if (!zipentry.isDirectory() && zipentry.getName().toLowerCase().contains(fileToFind)) {
                    try {
                        inputstream = zipfile.getInputStream(zipentry);
                    } catch (Exception ex) {
                    }
                    try {
                        tmp = zipfile.getInputStream(zipentry);
                    } catch (Exception ex) {
                    }
                    encoding = com.tonsquemike.Archivos.checkEncoding(tmp);

                    str = new java.util.Scanner(inputstream, encoding).useDelimiter("\\A").next();
                    if (!labelToFind.equals(""))
                        token = findLabelOnTxtFile(str.toLowerCase(), labelToFind.toLowerCase());
                }
            }catch (Exception ex){ex.getStackTrace();}
        }
        if(out.equals(""))
            System.out.print(token);
        else
            Archivos.escribeArchivo(out, str);
    }

    public String checkEncoding(String fileName) {
        byte[] buf = new byte[4096];
        String encoding = "null";

        try {
            FileInputStream fileInputStream = new FileInputStream(fileName);
            UniversalDetector detector = new UniversalDetector((CharsetListener)null);

            int nread;
            while((nread = fileInputStream.read(buf)) > 0 && !detector.isDone()) {
                detector.handleData(buf, 0, nread);
            }

            detector.dataEnd();
            encoding = detector.getDetectedCharset();
            encoding = Encodings.toJavaEncoding(encoding);
            detector.reset();
            fileInputStream.close();
        } catch (IOException var6) {
            var6.printStackTrace();
        }

        return encoding;
    }
    public String findLabelOnTxtFile(String content, String label){
        content = content.replaceAll("\t"," ");
        String     out = "nil";
        label          = label+" ";
        String []lines = content.split("\n");

        //System.out.println("buscando "+label);
        for (int i = 0; i < lines.length; i++) {
            //System.out.println("line = "+lines[i].toLowerCase());
            if(lines[i].toLowerCase().contains(label)) {//modificado desde --contains->startsWith
                out = lines[i];
                //System.out.println("ENTRÖ "+out);
            }
        }
        out = out.split(" ")[1];
        return Character.toUpperCase(out.charAt(0)) + out.substring(1);
    }
}