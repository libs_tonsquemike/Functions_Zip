package com.tonsquemike;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.mozilla.universalchardet.CharsetListener;
import org.mozilla.universalchardet.UniversalDetector;

public class Archivos {
    public Archivos() {
    }

    public static void main(String[] args) {
        checkEncoding("");
    }

    public static int cantidadDePalabras(String nombreArchivo) {
        int i = 0;

        try {
            File archivo = new File(nombreArchivo);
            FileReader fr = new FileReader(archivo);

            BufferedReader br;
            for(br = new BufferedReader(fr); br.readLine() != null; ++i) {
                ;
            }

            fr.close();
            br.close();
            return i;
        } catch (Exception var7) {
            var7.printStackTrace();
            return 0;
        }
    }

    public static String[] leerArchivoDeTexto(String nombreArchivo) {
        int i = 0;

        try {
            File archivo = new File(nombreArchivo);
            FileReader fr = new FileReader(archivo);

            BufferedReader br;
            for(br = new BufferedReader(fr); br.readLine() != null; ++i) {
                ;
            }

            fr.close();
            br.close();
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);
            String[] lineas = new String[i];

            String linea;
            for(i = 0; (linea = br.readLine()) != null; lineas[i++] = linea) {
                ;
            }

            fr.close();
            br.close();
            return lineas;
        } catch (Exception var8) {
            var8.printStackTrace();
            return null;
        }
    }

    public static String leerArchivoTexto(String nombreArchivo) {
        String content = "";

        try {
            content = new String(Files.readAllBytes(Paths.get(nombreArchivo)));
        } catch (IOException var3) {
            System.out.println("error reading " + nombreArchivo);
        }

        return content;
    }

    public static boolean escribeArchivo(String nombreArchivo, String[] contenido) {
        try {
            File ar = new File(nombreArchivo);
            FileWriter fr = new FileWriter(ar);
            BufferedWriter br = new BufferedWriter(fr);

            for(int i = 0; i < contenido.length - 1; ++i) {
                if (contenido.length >= 1 && contenido[contenido.length - 1] != null) {
                    br.write(contenido[i] + "\r\n");
                }
            }

            br.close();
            fr.close();
            return true;
        } catch (Exception var6) {
            var6.printStackTrace();
            return false;
        }
    }

    public static boolean escribeArchivo(String nombreArchivo, String cadena) {
        try {
            File ar = new File(nombreArchivo);
            FileWriter fr = new FileWriter(ar);
            BufferedWriter br = new BufferedWriter(fr);
            br.write(cadena + "\r\n");
            br.close();
            fr.close();
            return true;
        } catch (Exception var6) {
            var6.printStackTrace();
            return false;
        }
    }

    public static BufferedWriter addLine(BufferedWriter br, String cadena) {
        try {
            br.write(cadena + "\r\n");
            return br;
        } catch (Exception var3) {
            var3.printStackTrace();
            return null;
        }
    }

    public static boolean saveFile(BufferedWriter br) {
        try {
            br.close();
            return true;
        } catch (Exception var2) {
            var2.printStackTrace();
            return false;
        }
    }

    public static BufferedWriter newBuffer(String nomArch) {
        try {
            File ar = new File(nomArch);
            FileWriter fw = new FileWriter(ar);
            BufferedWriter bw = new BufferedWriter(fw);
            return bw;
        } catch (Exception var4) {
            var4.printStackTrace();
            return null;
        }
    }

    public static void copyTxt(String fuente, String destino) {
        try {
            String[] cadena = leerArchivoDeTexto(fuente);
            escribeArchivo(fuente, destino);
        } catch (Exception var3) {
            var3.getMessage();
        }

    }

    public static String checkEncoding(String fileName) {
        byte[] buf = new byte[4096];
        String encoding = "null";

        try {
            FileInputStream fileInputStream = new FileInputStream(fileName);
            UniversalDetector detector = new UniversalDetector((CharsetListener)null);

            int nread;
            while((nread = fileInputStream.read(buf)) > 0 && !detector.isDone()) {
                detector.handleData(buf, 0, nread);
            }

            detector.dataEnd();
            encoding = detector.getDetectedCharset();
            encoding = Encodings.toJavaEncoding(encoding);
            detector.reset();
            fileInputStream.close();
        } catch (IOException var6) {
            var6.printStackTrace();
        }

        return encoding;
    }

    public static String checkEncoding(InputStream inputStream) {
        byte[] buf = new byte[4096];
        InputStream fileInputStream = inputStream;
        String encoding = "null";

        try {
            UniversalDetector detector = new UniversalDetector((CharsetListener)null);

            int nread;
            while((nread = fileInputStream.read(buf)) > 0 && !detector.isDone()) {
                detector.handleData(buf, 0, nread);
            }

            detector.dataEnd();
            encoding = detector.getDetectedCharset();
            encoding = Encodings.toJavaEncoding(encoding);
            detector.reset();
            fileInputStream.close();
        } catch (IOException var6) {
            var6.printStackTrace();
        }

        return encoding;
    }
}

